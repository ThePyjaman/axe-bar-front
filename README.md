# Axe Bar Website

###### (nom temporaire)

## Lancer le projet

### Via docker compose (repo pas a jour)
récupérer le fichier docker-compose et l'excécuter dans un dossier à part

```shell
docker-compose up
```

### via le terminal
dans le dossier back :
```shell
npm test
```
dans le dossier front : 
```shell
npm start
```
### accéder au site déployé
[Par-ici!](https://www.thepyjaman.com)

## Structure du projet

## Arborescence (en cours de défintion)

* '/' : Page d'accueil ou l'on peut voir pour l'instant les évèements prévus dans le bar 
* '/presentation' : Page Présentation ou l'ou peut voir un descriptif du bar 
* '/reservation' : Page reservation ou l'on peut voir les disponibilités et réserver un créneau pour lancer des haches, ou s'insicrire a un évènements, ou privatiser le bar une parem par exemple
* '/food' : Page de présentation du menu
* '/authentication' : Page de Login Signup (basé firebase)
* '/event-list' : liste des events du point de vu administrateur(TODO : accessible seulement aux administrauteurs)
* '/event-form' : formulaire de création et d'édition devent 



### Authentification(pas encore fait)

#### Privilèges

le site possède un système d'utilisateurs à privilèges, qui sont au nombre de 5 :

* Créer/Supprimer/modifier les Evènements
* Modifier les privilèges des autres utilisateurs (impossible de donner des droits que l'on ne possède pas)
* Bannir des utilisateurs

#### Compte Root

Il existe un compte root qui possède tous les privilèges et ne peut pas les perdre.

```Json
email: root@root.fr
password: 12345
```
