import React, {useEffect} from 'react'
import {useDispatch} from "react-redux";
import {getEvents} from "./Redux/actions/events";
import Layout from "./pages/Layout";
import {BrowserRouter as Router} from "react-router-dom";
import {CssBaseline} from "@material-ui/core";

const App = () => {

    const dispatch = useDispatch()

    useEffect(() => {
        console.log('App.js fetch all data')
        dispatch(getEvents());
    }, [dispatch])

    return (
        <Router>
            <CssBaseline />
            <Layout/>
        </Router>

    )
}
export default App;