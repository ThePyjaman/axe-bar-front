import React from "react";
import {Route, Switch, useRouteMatch} from "react-router-dom";
import EventFormPage from "../pages/EventFormPage/EventFormPage";


const EventFormRouter = () => {

    let match = useRouteMatch();

    return (
        <Switch>
            <Route path={`${match.path}/:id`}>
                <EventFormPage/>
            </Route>
            <Route path={match.path}>
                <EventFormPage/>
            </Route>
        </Switch>
    )
}
export default EventFormRouter