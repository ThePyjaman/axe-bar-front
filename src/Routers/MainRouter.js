import React from "react";
import {Route, Switch} from "react-router-dom";
import EventListPage from "../pages/EventListPage/EventListPage";
import LandingPage from "../pages/LandingPage/LandingPage";
import EventFormRouter from "./EventFormRouter";
import FoodPage from "../pages/FoodPage/FoodPage";
import CalendarPage from "../pages/CalendarPage/CalendarPage";
import PresentationPage from "../pages/PresentationPage/PresentationPage";
import ReservationPage from "../pages/ReservationPage/ReservationPage";
import PageNotFound from "../pages/PageNotFound/PageNotFound";
import AuthenticationPage from "../pages/AuthenticationPage/AuthenticationPage";

const MainRouter = () => {

    return (
        <Switch>
            <Route exact path="/">
                <LandingPage/>
            </Route>
            <Route path="/event-form">
                <EventFormRouter/>
            </Route>
            <Route path="/event-list">
                <EventListPage/>
            </Route>
            <Route path="/food">
                <FoodPage/>
            </Route>
            <Route path="/calendar">
                <CalendarPage/>
            </Route>
            <Route path="/presentation">
                <PresentationPage/>
            </Route>
            <Route path="/reservation">
                <ReservationPage/>
            </Route>
            <Route path="/authentication">
                <AuthenticationPage/>
            </Route>
            <Route path="*">
                <PageNotFound/>
            </Route>
        </Switch>
    )
}
export default MainRouter