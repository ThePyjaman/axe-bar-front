import React, {useEffect} from "react";
import EventsCarousel from "../../components/Events/Carousel/EventsCarousel";
import {useDispatch} from "react-redux";
import {getEvents} from "../../Redux/actions/events";


const LandingPage = () => {

    const dispatch = useDispatch()

    useEffect(() => {
        console.log('LandingPage fetch all data')
        dispatch(getEvents());
    }, [dispatch])

    return (
        <EventsCarousel/>
    )
}
export default LandingPage;