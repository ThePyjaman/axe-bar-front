import React, {useEffect} from "react";
import {useDispatch} from "react-redux";
import {getEvents} from "../../Redux/actions/events";
import EventList from "../../components/Events/List/EventList";


const EventListPage = () => {

    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(getEvents());
    }, [dispatch])


    return (
        <EventList/>
    )
}
export default EventListPage;