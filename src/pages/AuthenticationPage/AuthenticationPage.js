import React from "react";
import {Divider, Grid} from "@material-ui/core";
import SignInForm from "../../components/Authentication/SignInForm";
import SignUpForm from "../../components/Authentication/SignUpForm";


const AuthenticationPage = () => {

    return(
        <Grid container maxWidth="lg">
            <SignUpForm />
            <Divider orientation="vertical" flexItem />
            <SignInForm />
        </Grid>
    )

}
export default AuthenticationPage