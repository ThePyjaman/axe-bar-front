import React, {useEffect} from "react";
import {Container, Grid} from "@material-ui/core";
import {useDispatch} from "react-redux";
import {getEvents} from "../Redux/actions/events";
import NavigationBar from "../components/NavigationBar/NavigationBar";
import MainRouter from "../Routers/MainRouter";


const Layout = () => {

    const dispatch = useDispatch()
    useEffect(() => {
        dispatch(getEvents());
    }, [dispatch])


    return (
        <Grid>
            <NavigationBar/>
            <Container maxWidth="lg">
                <MainRouter/>
            </Container>
        </Grid>

    )
}
export default Layout;