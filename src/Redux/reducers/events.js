import {CREATE, DELETE, FETCH_ALL, UPDATE} from "../../assets/constants/actionTypes";

// eslint-disable-next-line
export default (events = [], action) => {
    switch (action.type) {
        case FETCH_ALL:
            console.log('fetching all data')
            return action.payload;
        case CREATE:
            console.log('creating event')
            return [...events, action.payload];
        case UPDATE:
            console.log('Updating event')
            return events.map((event) => (event._id === action.payload._id ? action.payload : event));
        case DELETE:
            console.log('Deleing an event')
            return events.filter((event) => event._id !== action.payload)
        default:
            console.log('default behaviour')
            return events;
    }
}