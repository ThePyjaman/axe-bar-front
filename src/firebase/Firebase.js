import firebase from "firebase/app";
import "firebase/auth"

const firebaseConfig = {
    apiKey: "AIzaSyDGiC1Ge4l9d_EO7WbrU5cnXWMad81S6y0",
    authDomain: "axe-throwing-site.firebaseapp.com",
    projectId: "axe-throwing-site",
    storageBucket: "axe-throwing-site.appspot.com",
    messagingSenderId: "869297349368",
    appId: "1:869297349368:web:8d11538913c3471c23110d",
    measurementId: "G-EZYJXBWDDJ"
};

const app = firebase.initializeApp(firebaseConfig)
export const auth = app.auth()

export default app

