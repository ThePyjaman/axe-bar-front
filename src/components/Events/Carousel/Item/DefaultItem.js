import React from "react";
import useStyles from './styles'
import {Grid, Paper, Typography} from '@material-ui/core'
import loneliness from '../../../../assets/images/loliness.jpg'
import {createMuiTheme, responsiveFontSizes, ThemeProvider} from "@material-ui/core/styles";

const Item = () => {

    let theme = createMuiTheme();
    theme = responsiveFontSizes(theme);

    const classes = useStyles();

    return (
        <Paper elevation={10} className={classes.mainFeaturedPost}
               style={{backgroundImage: loneliness}}>
            <div className={classes.overlay}/>
            <Grid container>
                <Grid item md={6}>
                    <div className={classes.mainFeaturedPostContent}>
                        <ThemeProvider theme={theme}>
                            <Typography variant="h3" color="inherit" gutterBottom>
                                Pas d'évènements prévu pour l'instant
                            </Typography>
                        </ThemeProvider>
                    </div>
                </Grid>
            </Grid>
        </Paper>
    );
}
export default Item;