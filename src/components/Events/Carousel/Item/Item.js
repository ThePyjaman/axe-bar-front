import React from "react";
import useStyles from './styles'
import {Button, CardActions, Grid, Paper, Typography,} from '@material-ui/core'
import {createMuiTheme, responsiveFontSizes, ThemeProvider} from '@material-ui/core/styles';
import ShareIcon from '@material-ui/icons/Share';


const Item = (props) => {

    let theme = createMuiTheme();
    theme = responsiveFontSizes(theme);
    const classes = useStyles();

    return (
        <Paper elevation={10} className={classes.mainFeaturedPost}
               style={{backgroundImage: `url(${props.event.selectedFile})`}}>
            <div className={classes.overlay}/>
            <Grid container>
                <Grid item md={6}>
                    <div className={classes.mainFeaturedPostContent}>
                        <ThemeProvider theme={theme}>
                            <Typography variant="h3" color="inherit" gutterBottom>
                                {props.event.title}
                            </Typography>
                            <Typography variant="h5" color="inherit" paragraph>
                                {props.event.message}
                            </Typography>
                        </ThemeProvider>
                    </div>
                </Grid>
            </Grid>
            <div className={classes.bottomBar}>
                <CardActions style={{justifyContent: 'flex-end'}}>
                    <Button size="small" color="inherit" variant="outlined">
                        En savoir plus
                    </Button>
                    <Button size="small" color="inherit">
                        <ShareIcon/>
                    </Button>
                </CardActions>
            </div>
        </Paper>
        /*
        <Box my={4}>
            <FiCard>
                <FiCardMedia
                    media="picture"
                    alt="Event picture"
                    image={props.event.selectedFile}
                    title="Event Picture"
                />
                <FiCardContent className={classes.fiCardContent}>
                    <Typography gutterBottom variant="h5" component="h2">
                        {props.event.title}
                    </Typography>
                    <Typography gutterBottom variant="h6" component="h3">
                        {props.event.message}
                    </Typography>
                    <Typography
                        variant="body2"
                        className={classes.fiCardContentTextSecondary}
                        component="p"
                    >
                        {moment(props.event.createdAt).fromNow()}
                    </Typography>
                </FiCardContent>
                <FiCardActions className={classes.fiCardContent}>
                    <Button size="small" color="inherit" variant="outlined">
                        Share à faire
                    </Button>
                    <Button size="small" color="inherit" variant="outlined">
                        Link à faire
                    </Button>
                </FiCardActions>
            </FiCard>
        </Box>

         */
    )
}
export default Item;