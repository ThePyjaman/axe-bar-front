import React from "react";
import {useSelector} from "react-redux";
import Carousel from 'react-material-ui-carousel'
import Item from "./Item/Item";
import DefaultItem from "./Item/DefaultItem";


const EventsCarousel = () => {

    const events = useSelector((state) => state.events)

    if (events.length === 0) {
        return (<DefaultItem/>)
    }
    return (
        <Carousel
            className='carousel'
            autoPlay={true}
            animation="slide"
            timeout={500}
            cycleNavigation={true}
            navButtonsAlwaysVisible={false}
            navButtonsAlwaysInvisible={true}

        >
            {
                events.map((item, i) => (
                    <Item event={item} key={i}/>
                ))
            }
        </Carousel>
    )
}
export default EventsCarousel