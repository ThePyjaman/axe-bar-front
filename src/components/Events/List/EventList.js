import React from "react";
import {useSelector} from "react-redux";
import {Grid, Grow} from "@material-ui/core";
import Item from "./Item/Item";
import useStyles from "./Item/styles";
import DefaultItem from "./Item/DefaultItem";


const EventList = () => {

    const classes = useStyles();
    const events = useSelector((state) => state.events)

    if (events.length === 0) {
        return (<DefaultItem/>)
    }

    return (
        !events.length ? <DefaultItem/> : (
            <Grow in>
                <Grid className={classes.container} container alignItems='stretch' spacing={3}>
                    {events.map((event) => (
                        <Grid key={event._id} item xs={12} sm={6} md={6}>
                            <Item event={event}/>
                        </Grid>
                    ))}
                </Grid>
            </Grow>
        )
    )
}
export default EventList