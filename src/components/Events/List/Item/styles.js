import {makeStyles} from '@material-ui/core/styles';

export default makeStyles({
    root: {},
    media: {
        height: 140,
    },
    overlay2: {
        position: 'absolute',
        top: '20px',
        right: '20px',
        color: 'white',
    },
});