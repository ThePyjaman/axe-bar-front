import React from "react";
import useStyles from './styles'
import {Card, CardActionArea, CardContent, CardMedia, Typography} from '@material-ui/core'
import loneliness from '../../../../assets/images/loliness.jpg'

const Item = () => {

    const classes = useStyles();

    return (
        <Card className={classes.root}>
            <CardActionArea>
                <CardMedia
                    className={classes.media}
                    media="picture"
                    alt="Event picture"
                    image={loneliness}
                    title="Event Picture"
                />
                <CardContent>
                    <Typography gutterBottom variant="h5" component="h2">
                        Pas d'évènements prévus
                    </Typography>
                </CardContent>
            </CardActionArea>
        </Card>
    );
}
export default Item;