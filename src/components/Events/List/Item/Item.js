import React from "react";
import useStyles from './styles'
import {Button, Card, CardActionArea, CardActions, CardContent, CardMedia, Typography} from '@material-ui/core'
import moment from "moment";
import {useDispatch} from 'react-redux'
import {useHistory} from "react-router-dom";

import {deleteEvent} from '../../../../Redux/actions/events'

const Item = (props) => {

    const history = useHistory()
    const classes = useStyles();
    const dispatch = useDispatch()

    return (
        <Card className={classes.root}>
            <CardActionArea>
                <CardMedia
                    className={classes.media}
                    media="picture"
                    alt="Event picture"
                    image={props.event.selectedFile}
                    title="Event Picture"
                />
                <CardContent>
                    <Typography gutterBottom variant="h5" component="h2">
                        {props.event.title}
                    </Typography>
                    <Typography variant="body2" color="textSecondary" component="p">
                        Posted {moment(props.event.createdAt).fromNow()}
                    </Typography>
                </CardContent>
            </CardActionArea>
            <CardActions>
                <Button size="small" color="primary"
                        onClick={() => {
                            history.push(`/event-form/${props.event._id}`)
                        }}
                >
                    Update
                </Button>
                <Button size="small" color="primary"
                        onClick={() => {
                            dispatch(deleteEvent(props.event._id))
                            history.push('/event-list')
                        }}
                >
                    Delete
                </Button>
            </CardActions>
        </Card>
    );
}
export default Item;