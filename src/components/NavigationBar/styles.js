import {makeStyles} from '@material-ui/core/styles';

export default makeStyles({
    navDisplayFlex: {
        display: `flex`,
        justifyContent: `space-between`
    },
    linkText: {
        textDecoration: `none`,
        textTransform: `uppercase`,
        color: `white`
    },
    itemText: {
        textAlign: 'center',
    },
    userInfo: {
        display: `flex`,
        justifyContent: 'align-left'
    },
    root: {
        flexGrow: 1,
    },
    toolbarButtons: {
        marginLeft: 'auto',
    },
});