import React, {useEffect, useState} from "react";
import useStyles from './styles'
import {Button, Checkbox, Container, FormControlLabel, Grid, Link, TextField, Typography} from "@material-ui/core";

const SignInForm = () => {

    const classes = useStyles();

    const [AuthData, setAuthData] = useState({
        identifier: '',
        password: '',
        rememberMe: false,
    });

    useEffect(() => {

    }, [AuthData ])

    return (
        <Container component="main" maxWidth="xs">
            <div className={classes.paper}>
                <Typography component="h1" variant="h5">
                    Connexion
                </Typography>
                <form className={classes.form} noValidate>
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        id="identifier"
                        label="Identifiant / Email"
                        name="identifier"
                        autoComplete="identifier"
                        autoFocus
                    />
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        name="password"
                        label="Password"
                        type="password"
                        id="password"
                        autoComplete="current-password"
                    />
                    <FormControlLabel
                        control={<Checkbox value="remember" color="primary" />}
                        label="Remember me"
                    />
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                    >
                        Sign In
                    </Button>
                        <Grid item xs>
                            <Link href="#" variant="body2">
                                Forgot password?
                            </Link>
                        </Grid>
                </form>
            </div>
        </Container>
    );
}
export default SignInForm